import tensorflow as tf
from tensorflow.keras.callbacks import ModelCheckpoint, TensorBoard, LearningRateScheduler
from tensorflow.keras.optimizers import SGD, Adam
import augmentation
from ssd_loss import SSDLoss
from utils import bbox_utils, data_utils, io_utils, train_utils
from models.ssd_mobilenet_v2 import get_model, init_model
from os import listdir
from os.path import isfile, join

def parse_tfrecord(tfrecord , class_table):
  x = tf.io.parse_single_example(tfrecord, IMAGE_FEATURE_MAP)
  x_train = tf.image.decode_jpeg(x['image/encoded'] )
  
  x_train = tf.image.convert_image_dtype(x_train, tf.float32)
  x_train = tf.image.resize(x_train, (300 , 300))

  class_text = tf.sparse.to_dense(x['image/object/class/text'], default_value='')
  # print(class_text)
  labels = tf.cast(class_table.lookup(class_text)+1, tf.int32)
  # print(labels)
  y_train = tf.stack([tf.sparse.to_dense(x['image/object/bbox/ymin']),
                      tf.sparse.to_dense(x['image/object/bbox/xmin']),
                      tf.sparse.to_dense(x['image/object/bbox/ymax']),
                      tf.sparse.to_dense(x['image/object/bbox/xmax'])], axis=1)
  
  img, gt_boxes = augmentation.apply(x_train, y_train)
  
  return img , gt_boxes , labels

def get_compiled_model():
    # Make a simple 2-layer densely-connected neural network.
    ssd_model = get_model(hyper_params)
    # ssd_custom_losses = CustomLoss(hyper_params["neg_pos_ratio"], hyper_params["loc_loss_alpha"])
    
    ssd_loss = SSDLoss(neg_pos_ratio=3, alpha=1.0)
    ssd_model.compile(optimizer=Adam(learning_rate=1e-4),
                  loss=ssd_loss.compute_loss)
    init_model(ssd_model)
    
    return ssd_model

def get_dataset():
    batch_size = 32
    hyper_params = train_utils.get_hyper_params(backbone)
    prior_boxes = bbox_utils.generate_prior_boxes(hyper_params["feature_map_shapes"], hyper_params["aspect_ratios"])
    ssd_train_feed = train_utils.generator(train_data, prior_boxes, hyper_params)
    
    return (ssd_train_feed)

batch_size = 32
epochs = 25
backbone = "mobilenet_v2"
io_utils.is_valid_backbone(backbone)
hyper_params = train_utils.get_hyper_params(backbone)

labels = [ 'aeroplane', 'bicycle', 'bird', 'boat', 'bottle', 'bus', 'car',
 'cat', 'chair', 'cow', 'diningtable', 'dog', 'horse', 'motorbike', 'person',
 'pottedplant', 'sheep', 'sofa', 'train', 'tvmonitor']

labels = ["bg"] + labels
hyper_params["total_labels"] = len(labels)
img_size = hyper_params["img_size"]

mypath = 'TfRecord'
onlyfiles = ['TfRecord/'+f for f in listdir(mypath) if isfile(join(mypath, f))]
class_file = 'dataset.names'
LINE_NUMBER = -1
files = tf.data.Dataset.list_files(onlyfiles)
dataset = files.flat_map(tf.data.TFRecordDataset)
class_table = tf.lookup.StaticHashTable(tf.lookup.TextFileInitializer(class_file, tf.string, 0, tf.int64, LINE_NUMBER, delimiter="\n"), -1)

IMAGE_FEATURE_MAP = {
    'image/encoded': tf.io.FixedLenFeature([], tf.string),
    'image/object/bbox/xmin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymin': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/xmax': tf.io.VarLenFeature(tf.float32),
    'image/object/bbox/ymax': tf.io.VarLenFeature(tf.float32),
    'image/object/class/text': tf.io.VarLenFeature(tf.string),
    'image/height': tf.io.VarLenFeature(tf.int64),
    'image/width': tf.io.VarLenFeature(tf.int64),
}

temp = dataset.map(lambda x: parse_tfrecord(x , class_table ))
data_shapes = data_utils.get_data_shapes()
padding_values = data_utils.get_padding_values()
train_data = temp.shuffle(batch_size*4).padded_batch(batch_size, padded_shapes=data_shapes, padding_values=padding_values)

strategy = tf.distribute.MirroredStrategy()
print("Number of devices: {}".format(strategy.num_replicas_in_sync))

# Open a strategy scope.
with strategy.scope():
    model = get_compiled_model()

train_data_set = get_dataset()

model.summary()
model.fit(train_data_set ,steps_per_epoch = 155, epochs= epochs)

model.save_weights("final_weights.h5")
